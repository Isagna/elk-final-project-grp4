const fs = require ('fs');


let finalData = []


function generateIndexedJSON(path, finalData) {

    var jsonPath = path;

    let file = fs.readFileSync(jsonPath).toString()
    let datas = JSON.parse(file)['statuses'];

    datas.forEach(
        data => {
            let index = JSON.parse('{"index": {"_index": "tweet"}}')
            finalData.push(index)
            finalData.push(data)
        })
}

for(let i = 1; i < 11; i++) {
    let filePath = "./import_data/data_from_twitter/" + i + "_larem.json"
    generateIndexedJSON(filePath, finalData);
}

for(let i = 1; i < 11; i++) {
    let filePath = "./import_data/data_from_twitter/" + i + "_macron.json"
    generateIndexedJSON(filePath, finalData);
}

for(let i = 1; i < 11; i++) {
    let filePath = "./import_data/data_from_twitter/before_election/" + i + "_larem.json"
    generateIndexedJSON(filePath, finalData);
}

for(let i = 1; i < 11; i++) {
    let filePath = "./import_data/data_from_twitter/before_election/" + i + "_macron.json"
    generateIndexedJSON(filePath, finalData);
}


let jsonString = JSON.stringify(finalData)


fs.writeFile('./import_data/data_to_export/data.json', jsonString, err => {
    if (err) {
        console.log('Error writing file', err)
    } else {
        console.log('Successfully wrote file')
    }
})

